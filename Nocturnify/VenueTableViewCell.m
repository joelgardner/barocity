//
//  VenueTableViewCell.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VenueTableViewCell.h"

@implementation VenueTableViewCell

@synthesize name = _name, address = _address, description = _description, image = _image;

- (id)initWithName:(NSString *)name address:(NSString *)address
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VenueCell"];
    if (self)
    {
        self.name.text = name;
        self.address.text = address;
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
