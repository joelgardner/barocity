//
//  AlertsViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/18/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertsViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *alerts;

@end
