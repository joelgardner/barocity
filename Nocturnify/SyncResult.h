//
//  SyncResult.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/13/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum 
{
    IDLE,
    SYNCING,
    SYNC_FAILED,
    SYNC_SUCCEEDED
} SyncStatus;

@interface SyncResult : NSObject

@property (nonatomic, strong) NSString *error;
@property SyncStatus syncStatus;

@end
