//
//  SyncController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/22/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "SyncResult.h"

@class Login;

@protocol SyncControllerDelegate <NSObject>

- (void)syncDidFinish:(SyncResult *)syncResult;
- (void)locationUpdatedWithLatitude:(double)latitude withLongitude:(double)longitude;

@end

@interface SyncController : NSObject <CLLocationManagerDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, strong) id<SyncControllerDelegate> delegate;
- (Login *)verifyUsername:(NSString *)username withPassword:(NSString *)password;
- (void)sync;
- (void)syncWithLatitude:(double)latitude withLongitude:(double)longitude;
- (void)syncDeviceId:(NSString *)deviceId;
- (void)syncLocation;
@end
