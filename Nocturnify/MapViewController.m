//
//  MapViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/21/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "MapViewController.h"
#import "LogHelper.h"

@implementation MapViewController

@synthesize mapView = _mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [LogHelper logMessage:@"END: asdfsadfsadfasdf VIEWDIDLOAD"];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"Map";
    titleLabel.font = [UIFont fontWithName:@"Heiti SC" size: 22.0];
    titleLabel.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = titleLabel;
    
    //30.267815,-97.745533
    CLLocationCoordinate2D latlng = CLLocationCoordinate2DMake(30.267815, -97.745533);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.01, 0.01);
    self.mapView.region = MKCoordinateRegionMake(latlng, span);
    self.mapView.delegate = self;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
