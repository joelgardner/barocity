//
//  VenueFilterContainerTableViewCell.h
//  Barocity
//
//  Created by Joel Gardner on 11/13/12.
//
//

#import <UIKit/UIKit.h>

@interface VenueFilterContainerTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *featureName;
@property BOOL userSelected;
@end
