//
//  VenueSpecialtyDrink.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface VenueSpecialtyDrink : NSManagedObject

@property (nonatomic, retain) NSNumber * venue_id;
@property (nonatomic, retain) NSNumber * s_drink_id;
@property (nonatomic, retain) NSString * s_drink_name;
@property (nonatomic, retain) NSString * s_drink_desc;
@property (nonatomic, retain) NSDecimalNumber * s_drink_price;

@end
