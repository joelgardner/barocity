//
//  VenueWine.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/12/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface VenueWine : NSManagedObject

@property (nonatomic, retain) NSNumber * wine_id;
@property (nonatomic, retain) NSNumber * venue_id;
@property (nonatomic, retain) NSString * wine_type_code;
@property (nonatomic, retain) NSDecimalNumber * glass_price;
@property (nonatomic, retain) NSDecimalNumber * carafe_price;
@property (nonatomic, retain) NSDecimalNumber * bottle_price;
@property (nonatomic, retain) NSString * wine_desc;
@property (nonatomic, retain) NSString * wine_type_desc;
@property (nonatomic, retain) NSString * wine_name;

@end
