//
//  VenueFilterData.m
//  Nocturnify
//
//  Created by Joel Gardner on 10/28/12.
//
//

#import "VenueFilterData.h"

@implementation VenueFilterData

@synthesize feature = _feature, search_value = _search_value;

- (id)initWithFeature:(Feature *)f
{
    self = [super init];
    if (self)
    {
        self.feature = f;
        self.search_value = 0;
    }
    return self;
}

@end
