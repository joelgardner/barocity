//
//  Feature.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/27/12.
//
//

#import "Feature.h"


@implementation Feature

@dynamic id_;
@dynamic feature_name;
@dynamic feature_type;

@end
