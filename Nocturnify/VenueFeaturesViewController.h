//
//  VenueFeaturesViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/17/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueFeaturesViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *features;

@end
