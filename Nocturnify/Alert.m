//
//  Alert.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/18/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "Alert.h"

@implementation Alert

@synthesize _id = __id, name = _name, description = _description;

@end
