//
//  SyncData.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/12/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncData : NSObject

@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSString *jsonKey;
@property (nonatomic, strong) NSURL *url;
@property SEL saveObjectsSelector;
@property (nonatomic, strong) void (^saveData)(NSArray *data);
- (SyncData *)initWithJsonKey:(NSString *)jsonKey withURL:(NSURL *)url withSaveDataBlock:(void (^)(NSArray *data))saveData;
- (SyncData *)initWithJsonKey:(NSString *)jsonKey withURL:(NSURL *)url withSaveObjectsSelector:(SEL)saveObjectsSelector;

@end
