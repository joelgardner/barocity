//
//  SyncResult.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/13/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "SyncResult.h"

@implementation SyncResult

@synthesize error = _error, syncStatus = _syncStatus;

@end
