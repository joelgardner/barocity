//
//  SyncController.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/22/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
/*
 #ifdef DEBUG
#define BaseURL @"http://192.168.1.6:2121"
#else
#define BaseURL @"http://nocturnify.com/json"
#endif
#define VenuesURL [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseURL, @"/bars"]]
#define VenueBeersURL [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseURL, @"/bar_beers"]]
#define VenueWinesURL [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseURL, @"/bar_wines"]]
#define VenueMixedDrinksURL [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseURL, @"/bar_mixed_drinks"]]
#define VenueSpecialtyDrinksURL [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseURL, @"/bar_specialty_drinks"]]
#define FeaturesURL [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseURL, @"/features"]]
*/
 
#ifdef DEBUG
#define BaseURL @"http://nocturnify.com/json.php"
#else
#define BaseURL @"http://nocturnify.com/json.php"
#endif
#define VenuesURL [NSURL URLWithString: [NSString stringWithFormat:@"%@?t=%@", BaseURL, @"bars"]]
#define VenueBeersURL [NSURL URLWithString: [NSString stringWithFormat:@"%@?t=%@", BaseURL, @"bar_beers"]]
#define VenueWinesURL [NSURL URLWithString: [NSString stringWithFormat:@"%@?t=%@", BaseURL, @"bar_wines"]]
#define VenueMixedDrinksURL [NSURL URLWithString: [NSString stringWithFormat:@"%@?t=%@", BaseURL, @"bar_mixed_drinks"]]
#define VenueSpecialtyDrinksURL [NSURL URLWithString: [NSString stringWithFormat:@"%@?t=%@", BaseURL, @"bar_specialty_drinks"]]
#define FeaturesURL [NSURL URLWithString: [NSString stringWithFormat:@"%@?t=%@", BaseURL, @"features"]]
 
#import "SyncController.h"
#import "Venue.h"
#import "DataController.h"
#import "Login.h"
#import "LogHelper.h"
#import "SyncData.h"
#import "Config.h"

@interface SyncController (PrivateMethods)
- (void)saveSyncData;
@end

@implementation SyncController
{
    DataController *dataController;
    SyncResult *syncResult;
    CLLocationManager *locMgr;
    NSMutableData *receivedData;
}

@synthesize delegate = _delegate;

- (id)init
{
    self = [super init];
    if (self)
    {
        dataController = [[DataController alloc] init];
        syncResult = [[SyncResult alloc] init];
        locMgr = [[CLLocationManager alloc] init];
        receivedData = [[NSMutableData alloc] init];
    }
    
    return self;
}

- (Login *)verifyUsername:(NSString *)username withPassword:(NSString *)password;
{
    Login* login = [[Login alloc] init];
    login.syncId = 1;
    login.isSuccessful = YES;
    
    return login;
}

- (void)sync
{
    syncResult.syncStatus = SYNCING;
    
    // get the device id and clean it before sending to server
    NSString *deviceId = [Config deviceId];

    // get the the device's lat/long
    CLLocation *loc = [Config location];
    
    // build request and send the deviceid, lat, long to server
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:BaseURL]];
    NSString *body = [NSString stringWithFormat:@"deviceid=%@&lat=%f&lon=%f", deviceId, loc.coordinate.latitude, loc.coordinate.longitude];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: [NSData dataWithBytes:[body UTF8String] length:[body length]]];
    
    // execute request
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)syncDeviceId:(NSString *)deviceId
{
    // build request and send deviceId to the server
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://nocturnify.com/apn.php"]];
    NSString *body = [NSString stringWithFormat:@"deviceid=%@", deviceId];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: [NSData dataWithBytes:[body UTF8String] length:[body length]]];
    
    // execute request
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection.originalRequest.URL.description == BaseURL)
    {
        [self saveSyncData];
        syncResult.syncStatus = SYNC_SUCCEEDED;
        [self.delegate syncDidFinish:syncResult];
        [receivedData setLength:0];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection.originalRequest.URL.description == BaseURL)
    {
        [receivedData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //NSLog(@"ERROR: %@", error);
    //[responseData release];
    //[connection release];
    //[textView setString:@"Unable to fetch data"];
}

- (void)saveSyncData
{
    
    NSError* error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves|NSJSONReadingAllowFragments error:&error];
    //NSLog(@"%@, %@", json, error);
    
    // venues
    [dataController clearVenues];
    [dataController saveVenues:[json objectForKey:@"venues"]];
    
    // venue beers
    [dataController clearVenueBeers];
    [dataController saveVenueBeers:[json objectForKey:@"venue_beers"]];
    
    // venue wines
    [dataController clearVenueWines];
    [dataController saveVenueWines:[json objectForKey:@"venue_wines"]];
    
    // venue mixed drinks
    [dataController clearVenueMixedDrinks];
    [dataController saveVenueMixedDrinks:[json objectForKey:@"venue_mixed_drinks"]];
    
    // venue specialty drinks
    [dataController clearVenueSpecialtyDrinks];
    [dataController saveVenueSpecialtyDrinks:[json objectForKey:@"venue_specialty_drinks"]];
    
    // features
    [dataController clearFeatures];
    [dataController saveFeatures:[json objectForKey:@"features"]];
}

- (void)syncLocation
{
    if ([CLLocationManager locationServicesEnabled])
    {
        locMgr.delegate = self;
        [locMgr startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *loc = [locations lastObject];
    //NSLog(@"Location: %@", [locations lastObject]);
    
    // is this location update less than 30 seconds old?
    if ([loc.timestamp dateByAddingTimeInterval:30] > [NSDate date])
    {
        //NSLog(@"received new location data: %@", loc);
        
        // if so, stop receiving updates and signal our delegate that we got our location
        [manager stopUpdatingLocation];
        [self.delegate locationUpdatedWithLatitude:loc.coordinate.latitude withLongitude:loc.coordinate.longitude];
    }
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location Services error: %@", error);
}

- (void)syncDidFinish
{
    [self.delegate syncDidFinish:syncResult];
}

@end
