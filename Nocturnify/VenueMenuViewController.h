//
//  VenueMenuViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/17/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueMenuViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *menu;

- (id)initWithBeers:(NSMutableArray *)beers
          withWines:(NSMutableArray *)wines
    withMixedDrinks:(NSMutableArray *)mixedDrinks
withSpecialtyDrinks:(NSMutableArray *)specialtyDrinks;

@end
