//
//  DataController.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/13/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "DataController.h"
#import "Venue.h"
#import "VenueDeal.h"
#import "VenueFeature.h"
#import "VenueMenuItem.h"
#import "Alert.h"
#import "SyncController.h"
#import "AppDelegate.h"
#import "LogHelper.h"
#import "VenueBeer.h"
#import "VenueWine.h"
#import "VenueMixedDrink.h"
#import "VenueSpecialtyDrink.h"
#import "Feature.h"

@implementation DataController
{
    NSManagedObjectContext *context;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = appDelegate.managedObjectContext;
    }
    
    return self;
}

- (NSMutableArray *)getVenues
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Venue" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedVenues = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (Venue *v in fetchedVenues) 
    {
        [results addObject:v];
    }
    
    return results;
}

- (NSMutableArray *)getAllFeatures
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Feature" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedFeatures = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (Feature *f in fetchedFeatures)
    {
        [results addObject:f];
    }
    
    return results;
}

- (NSMutableArray *)getBeersForVenue:(int)_venueId
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // build predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(venue_id == %d)", _venueId];
    [fetchRequest setPredicate:predicate];
    
    // build entity description
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"VenueBeer" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
 
    // execute fetch request
    NSError *error;
    NSArray *fetchedVenueBeers = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (VenueBeer *vb in fetchedVenueBeers) 
    {
        [results addObject:vb];
    }

    return results;
}

- (NSMutableArray *)getWinesForVenue:(int)_venueId
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"VenueWine" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedVenueWines = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (VenueWine *vw in fetchedVenueWines)
    {
        [results addObject:vw];
    }
    
    return results;
}

- (NSMutableArray *)getMixedDrinksForVenue:(int)_venueId
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"VenueMixedDrink" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedVenueMixedDrinks = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (VenueMixedDrink *vmd in fetchedVenueMixedDrinks)
    {
        [results addObject:vmd];
    }
    
    return results;
}

- (NSMutableArray *)getSpecialtyDrinksForVenue:(int)_venueId
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"VenueSpecialtyDrink" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedVenueSpecialtyDrinks = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (VenueSpecialtyDrink *vsd in fetchedVenueSpecialtyDrinks)
    {
        [results addObject:vsd];
    }
    
    return results;
}

- (NSMutableArray *)getDeals:(int)_venueId
{
    VenueDeal *d = [[VenueDeal alloc] init];
    d._id = 1;
    d.title = @"$5 Appetizers for Happy Hour";
    d.description = @"All appetizers are $5 until 7PM, every day.";
    return [NSMutableArray arrayWithObject:d];
}

- (NSMutableArray *)getAttributes:(int)_venueId
{
    VenueFeature *va = [[VenueFeature alloc] init];
    va._id = 1;
    va.title = @"Pool Tables";
    va.description = @"This venue has pool tables";
    va.count = 1;
    return [NSMutableArray arrayWithObject:va];
}

- (NSMutableArray *)getMenu:(int)_venueId
{
    VenueMenuItem *steak = [[VenueMenuItem alloc] init];
    steak._id = 1;
    steak.name = @"Grassfed Ribeye";
    steak.description = @"Authentic grassfed beef - 12oz";
    steak.price = 24.50;
    return [NSMutableArray arrayWithObject:steak];
}

- (NSMutableArray *)getAlerts
{
    Alert *alert1 = [[Alert alloc] init];
    alert1._id = 1;
    alert1.name = @"$2 H-Bombs!";
    alert1.description = @"All H-Bombs $2 for 1 hour!";
    
    Alert *alert2 = [[Alert alloc] init];
    alert2._id = 2;
    alert2.name = @"Free Bar Food!";
    alert2.description = @"Your choice of appetizer when you show the bartender this alert.";
    
    return [NSMutableArray arrayWithObjects:alert1, alert2, nil];
}

- (NSMutableArray *)getVenuesWithSearchTerm:(NSString *)searchTerm
{
    Venue *v = [[Venue alloc] init];
    v.id_ = [NSNumber numberWithInt:1];
    v.name = @"Eddie V's";
    v.address = @"301 E 5th";
    v.city = @"Austin";
    v.zip = @"78703";
    v.state = @"TX";
    v.description_ = @"Excellent steak and steakfood.  Top notch ambiance.";
    //v.deals = [self getDeals:v._id];
    //v.features = [self getAttributes:v._id];
    //v.menu = [self getMenu:v._id];
    
    Venue *hangar = [[Venue alloc] init];
    hangar.id_ = [NSNumber numberWithInt:2];
    hangar.name = @"Hangar";
    hangar.address = @"511 4th";
    hangar.city = @"Austin";
    hangar.zip = @"78704";
    hangar.state = @"TX";
    //hangar.deals = [self getDeals:hangar._id];
    //hangar.features = [self getAttributes:hangar._id];
    //hangar.menu = [self getMenu:hangar._id];
    
    return [NSMutableArray arrayWithObjects:v, hangar, nil];
}

- (void)clearVenues
{
    NSFetchRequest *allVenues = [[NSFetchRequest alloc] init];
    [allVenues setEntity:[NSEntityDescription entityForName:@"Venue" inManagedObjectContext:context]];
    [allVenues setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *venues = [context executeFetchRequest:allVenues error:&error];
    for (NSManagedObject *venue in venues) 
    {
        [context deleteObject:venue];
    }
}

- (void)saveVenues:(NSArray *)venues
{
    Venue* managedVenue;
    for (id v in venues)
    {
        managedVenue = [NSEntityDescription insertNewObjectForEntityForName:@"Venue" inManagedObjectContext:context];
        managedVenue.id_ = [NSNumber numberWithInt:[NULL_TO_NIL([v objectForKey:@"id"]) intValue]];
        managedVenue.name = NULL_TO_NIL([v objectForKey:@"name"]);
        managedVenue.address = NULL_TO_NIL([v objectForKey:@"address"]);
        managedVenue.city = NULL_TO_NIL([v objectForKey:@"city"]);
        //managedVenue.state = NULL_TO_NIL([v objectForKey:@"state"]);
        managedVenue.phone = NULL_TO_NIL([v objectForKey:@"phone"]);
        managedVenue.description_ = NULL_TO_NIL([v objectForKey:@"description"]);
        NSError *error;
        if (![context save:&error]) 
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
}

- (void)clearVenueBeers
{
    NSFetchRequest *allVenueBeers = [[NSFetchRequest alloc] init];
    [allVenueBeers setEntity:[NSEntityDescription entityForName:@"VenueBeer" inManagedObjectContext:context]];
    [allVenueBeers setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *venueBeers = [context executeFetchRequest:allVenueBeers error:&error];
    for (NSManagedObject *venueBeer in venueBeers) 
    {
        [context deleteObject:venueBeer];
    }
}

- (void)saveVenueBeers:(NSArray *)venueBeers
{
    VenueBeer* managedVenueBeer;
    for (id vb in venueBeers)
    {
        managedVenueBeer = [NSEntityDescription insertNewObjectForEntityForName:@"VenueBeer" inManagedObjectContext:context];
        managedVenueBeer.venue_id = [NSNumber numberWithInt:[NULL_TO_NIL([vb objectForKey:@"b_key"]) intValue]];
        managedVenueBeer.beer_id = [NSNumber numberWithInt:[NULL_TO_NIL([vb objectForKey:@"beer_key"]) intValue]];
        managedVenueBeer.beer_name = NULL_TO_NIL([vb objectForKey:@"beer_name"]);
        managedVenueBeer.beer_type = NULL_TO_NIL([vb objectForKey:@"beer_type"]);
        managedVenueBeer.form_type = NULL_TO_NIL([vb objectForKey:@"form_type"]);
        managedVenueBeer.beer_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vb objectForKey:@"beer_price"])];
        managedVenueBeer.bucket_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vb objectForKey:@"bucket_price"])];
        managedVenueBeer.beer_type_desc = NULL_TO_NIL([vb objectForKey:@"beer_type_desc"]);
        
        NSError *error;
        if (![context save:&error]) 
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
}

- (void)clearVenueWines
{
    NSFetchRequest *allVenueWines = [[NSFetchRequest alloc] init];
    [allVenueWines setEntity:[NSEntityDescription entityForName:@"VenueWine" inManagedObjectContext:context]];
    [allVenueWines setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *venueWines = [context executeFetchRequest:allVenueWines error:&error];
    for (NSManagedObject *venueWine in venueWines) 
    {
        [context deleteObject:venueWine];
    }
}

- (void)saveVenueWines:(NSArray *)venueWines
{
    VenueWine* managedVenueWine;
    for (id vw in venueWines)
    {
        managedVenueWine = [NSEntityDescription insertNewObjectForEntityForName:@"VenueWine" inManagedObjectContext:context];
        managedVenueWine.venue_id = [NSNumber numberWithInt:[NULL_TO_NIL([vw objectForKey:@"b_key"]) intValue]];
        managedVenueWine.wine_id = [NSNumber numberWithInt:[NULL_TO_NIL([vw objectForKey:@"wine_key"]) intValue]];
        managedVenueWine.wine_name = NULL_TO_NIL([vw objectForKey:@"wine_name"]);
        managedVenueWine.wine_desc = NULL_TO_NIL([vw objectForKey:@"wine_desc"]);;
        managedVenueWine.wine_type_code = NULL_TO_NIL([vw objectForKey:@"wine_type_code"]);
        managedVenueWine.glass_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vw objectForKey:@"glass_price"])];
        managedVenueWine.carafe_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vw objectForKey:@"carafe_price"])];
        managedVenueWine.bottle_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vw objectForKey:@"bottle_price"])];
        managedVenueWine.wine_type_desc = NULL_TO_NIL([vw objectForKey:@"wine_type_desc"]);
        
        NSError *error;
        if (![context save:&error]) 
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
}

- (void)clearVenueMixedDrinks
{
    NSFetchRequest *allVenueMixedDrinks = [[NSFetchRequest alloc] init];
    [allVenueMixedDrinks setEntity:[NSEntityDescription entityForName:@"VenueMixedDrink" inManagedObjectContext:context]];
    [allVenueMixedDrinks setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *venueMixedDrinks = [context executeFetchRequest:allVenueMixedDrinks error:&error];
    for (NSManagedObject *venueMixedDrink in venueMixedDrinks) 
    {
        [context deleteObject:venueMixedDrink];
    }
}

- (void)saveVenueMixedDrinks:(NSArray *)venueMixedDrinks
{
    VenueMixedDrink* managedVenueMixedDrink;
    for (id vmd in venueMixedDrinks)
    {
        managedVenueMixedDrink = [NSEntityDescription insertNewObjectForEntityForName:@"VenueMixedDrink" inManagedObjectContext:context];
        managedVenueMixedDrink.venue_id = [NSNumber numberWithInt:[NULL_TO_NIL([vmd objectForKey:@"b_key"]) intValue]];
        managedVenueMixedDrink.mixed_drink_id = [NSNumber numberWithInt:[NULL_TO_NIL([vmd objectForKey:@"mixed_drink_key"]) intValue]];
        managedVenueMixedDrink.liquor_type = NULL_TO_NIL([vmd objectForKey:@"liquor_type"]);
        managedVenueMixedDrink.mixed_drink_name = NULL_TO_NIL([vmd objectForKey:@"mixed_drink_name"]);;
        managedVenueMixedDrink.single_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vmd objectForKey:@"single_price"])];
        managedVenueMixedDrink.double_indicator = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vmd objectForKey:@"double_indicator"])];
        managedVenueMixedDrink.double_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vmd objectForKey:@"double_price"])];

        NSError *error;
        if (![context save:&error]) 
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }    
}

- (void)clearVenueSpecialtyDrinks
{
    NSFetchRequest *allVenueSpecialtyDrinks = [[NSFetchRequest alloc] init];
    [allVenueSpecialtyDrinks setEntity:[NSEntityDescription entityForName:@"VenueSpecialtyDrink" inManagedObjectContext:context]];
    [allVenueSpecialtyDrinks setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *venueSpecialtyDrinks = [context executeFetchRequest:allVenueSpecialtyDrinks error:&error];
    for (NSManagedObject *venueSpecialtyDrink in venueSpecialtyDrinks) 
    {
        [context deleteObject:venueSpecialtyDrink];
    }
}

- (void)saveVenueSpecialtyDrinks:(NSArray *)venueSpecialtyDrinks
{
    VenueSpecialtyDrink* managedVenueSpecialtyDrink;
    for (id vsd in venueSpecialtyDrinks)
    {
        managedVenueSpecialtyDrink = [NSEntityDescription insertNewObjectForEntityForName:@"VenueSpecialtyDrink" inManagedObjectContext:context];
        managedVenueSpecialtyDrink.venue_id = [NSNumber numberWithInt:[NULL_TO_NIL([vsd objectForKey:@"b_key"]) intValue]];
        managedVenueSpecialtyDrink.s_drink_id = [NSNumber numberWithInt:[NULL_TO_NIL([vsd objectForKey:@"s_drink_key"]) intValue]];
        managedVenueSpecialtyDrink.s_drink_desc = NULL_TO_NIL([vsd objectForKey:@"s_drink_desc"]);
        managedVenueSpecialtyDrink.s_drink_name = NULL_TO_NIL([vsd objectForKey:@"s_drink_name"]);;
        managedVenueSpecialtyDrink.s_drink_price = [NSDecimalNumber decimalNumberWithString:NULL_TO_NIL([vsd objectForKey:@"s_drink_price"])];
        
        NSError *error;
        if (![context save:&error]) 
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    } 
}

- (void)clearFeatures
{
    NSFetchRequest *allFeatures = [[NSFetchRequest alloc] init];
    [allFeatures setEntity:[NSEntityDescription entityForName:@"Feature" inManagedObjectContext:context]];
    [allFeatures setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *features = [context executeFetchRequest:allFeatures error:&error];
    for (NSManagedObject *feature in features)
    {
        [context deleteObject:feature];
    }
}

- (void)saveFeatures:(NSArray *)features
{
    Feature* managedFeature;
    for (id f in features)
    {
        managedFeature = [NSEntityDescription insertNewObjectForEntityForName:@"Feature" inManagedObjectContext:context];
        managedFeature.id_ = [NSNumber numberWithInt:[NULL_TO_NIL([f objectForKey:@"f_key"]) intValue]];
        managedFeature.feature_name = NULL_TO_NIL([f objectForKey:@"feature_description"]);
        managedFeature.feature_type = NULL_TO_NIL([f objectForKey:@"feature_type"]);
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
}

- (NSMutableArray *)getGeneralFeatures
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Feature" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSMutableArray *ids = [[NSMutableArray alloc] initWithCapacity:8];
    int i, x[8] = { 1, 54, 56, 53, 10, 11, 12, 19 };
    for (i = 0;i < 8;++i)
    {
        [ids addObject:[[NSNumber alloc] initWithInt:x[i]]];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id_ in %@", ids];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedFeatures = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (Feature *f in fetchedFeatures)
    {
        [results addObject:f];
    }
    
    return results;
}


@end
