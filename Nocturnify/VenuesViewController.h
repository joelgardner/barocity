//
//  VenuesViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/2/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenuesViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray *venues;
@property (strong, nonatomic) NSMutableArray *filteredVenues;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

- (void)dismissKeyboard;
@end
