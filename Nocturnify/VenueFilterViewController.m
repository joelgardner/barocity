//
//  VenueFilterViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/27/12.
//
//

#import "VenueFilterViewController.h"
#import "Feature.h"
#import "VenueFeatureTableViewCell.h"
#import "VenueFilterContainerViewController.h"
#import "DataController.h"
#import "VenueFilterData.h"

@interface VenueFilterViewController ()

@end

@implementation VenueFilterViewController
{
    VenueFilterContainerViewController *generalController;
    DataController *dataController;
}

@synthesize features = _features, searchField = _searchField, scrollView = _scrollView, pageControl = _pageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        dataController = [[DataController alloc] init];
    }
    return self;
}

/*
- (void)loadView
{
    
}
*/
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIColor *color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-gunmetal.jpg"]];
    [self.view setBackgroundColor:color];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 3)];
    self.searchField.leftView = paddingView;
    self.searchField.leftViewMode = UITextFieldViewModeAlways;
    
    // a page is the width of the scroll view
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 4, self.scrollView.frame.size.height - 95);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    // set up page control
    self.pageControl.numberOfPages = 4;
    self.pageControl.currentPage = 0;
    
    [self loadScrollViewWithPage:0];
    
    //self.tableView.separatorColor = [UIColor clearColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0 || page > 3)
        return;
    
    if (page == 0 && generalController == nil)
    {
        // testing
        NSMutableArray *features = [dataController getGeneralFeatures];
        NSMutableArray *filterData = [[NSMutableArray alloc] initWithCapacity:features.count];
        for (Feature *f in features)
        {
            [filterData addObject:[[VenueFilterData alloc] initWithFeature:f]];
        }
        generalController = [[VenueFilterContainerViewController alloc] initWithFilterData:filterData];
        generalController.view.frame = CGRectMake(self.scrollView.frame.size.width * page, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height - 95);
        [self.scrollView addSubview:generalController.view];
    }
}

/*
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%d", [self.features count]);
    return 0;//[self.features count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Feature *feature = [self.features objectAtIndex:indexPath.row];
    VenueFeatureTableViewCell *cell = (VenueFeatureTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FeatureFilterCell"];
    
    if (cell == nil)
    {
        cell = [[VenueFeatureTableViewCell alloc] init];
    }
    
    cell.name.text = feature.feature_name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VenueFeatureTableViewCell *cell = (VenueFeatureTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VenueFeatureTableViewCell *cell = (VenueFeatureTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
