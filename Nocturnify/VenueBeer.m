//
//  VenueBeer.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/9/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueBeer.h"


@implementation VenueBeer

@dynamic venue_id;
@dynamic beer_id;
@dynamic beer_name;
@dynamic beer_type;
@dynamic form_type;
@dynamic beer_price;
@dynamic bucket_price;
@dynamic beer_type_desc;

@end
