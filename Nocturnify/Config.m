//
//  Config.m
//  Barocity
//
//  Created by Joel Gardner on 11/20/12.
//
//

#import "Config.h"

@implementation Config

// psuedo-property: deviceId
static NSData *_deviceId;
+ (NSString *)deviceId
{
    NSString *did = [[_deviceId description] stringByReplacingOccurrencesOfString:@" " withString:@""];
    did = [did stringByReplacingOccurrencesOfString:@"<" withString:@""];
    did = [did stringByReplacingOccurrencesOfString:@">" withString:@""];
    return did;
}
+ (void)setDeviceId:(NSData *)data { _deviceId = data; }

// psuedo-property: location
static CLLocation *_location;
+ (CLLocation *)location { return _location; }
+ (void)setLocation:(CLLocation *)location { _location = location; }

@end
