//
//  VenueMenuItem.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/13/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenueMenuItem : NSObject

@property int _id;
@property (strong) NSString *name;
@property (strong) NSString *description;
@property double price;

@end
