//
//  Alert.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/18/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Alert : NSObject

@property int _id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;

@end
