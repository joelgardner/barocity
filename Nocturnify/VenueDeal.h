//
//  VenueDeal.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/10/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenueDeal : NSObject

@property int _id;
@property (strong) NSString *title;
@property (strong) NSString *description;

@end
