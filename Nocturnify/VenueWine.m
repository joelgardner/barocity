//
//  VenueWine.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/12/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueWine.h"


@implementation VenueWine

@dynamic wine_id;
@dynamic venue_id;
@dynamic wine_type_code;
@dynamic glass_price;
@dynamic carafe_price;
@dynamic bottle_price;
@dynamic wine_desc;
@dynamic wine_type_desc;
@dynamic wine_name;

@end
