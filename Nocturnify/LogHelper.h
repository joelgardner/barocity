//
//  LogHelper.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/12/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Venue;

@interface LogHelper : NSObject

+ (void)logVenue:(Venue *)venue;
+ (void)logMessage:(NSString *)message;

@end
