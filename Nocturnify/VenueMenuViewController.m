//
//  VenueMenuViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/17/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueMenuViewController.h"
#import "VenueMenuTableViewCell.h"
#import "VenueMenuItem.h"
#import "DataController.h"
#import "VenueBeer.h"
#import "VenueWine.h"

@interface VenueMenuViewController (PrivateMethods)

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text;
- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text addSeparator:(BOOL)separator;
@end

@implementation VenueMenuViewController
{
    DataController *dataController;
    int y;
    UIScrollView *scrollView;
    NSMutableArray *beers;
    NSMutableArray *wines;
    NSMutableArray *mixedDrinks;
    NSMutableArray *specialtyDrinks;
    
}

@synthesize menu = _menu;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        dataController = [[DataController alloc] init];
    }
    return self;
}

-(id)initWithBeers:(NSMutableArray *)_beers withWines:(NSMutableArray *)_wines withMixedDrinks:(NSMutableArray *)_mixedDrinks withSpecialtyDrinks:(NSMutableArray *)_specialtyDrinks
{
    self = [super init];
    if (self)
    {
        dataController = [[DataController alloc] init];
        self->beers = _beers;
        self->wines = _wines;
        self->mixedDrinks = _mixedDrinks;
        self->specialtyDrinks = _specialtyDrinks;
    }
    return self;
}

- (id)initWithMenu:(NSMutableArray *)menu
{
    self = [super init];
    if (self) {
        // Custom initialization
        dataController = [[DataController alloc] init];
        self.menu = menu;
        NSLog(@"%d", [self.menu count]);
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)loadView
{
    //NSMutableArray *menu = [dataController getMenu:<#(int)#>];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 100)];
    scrollView.showsVerticalScrollIndicator = YES;
    self.view = scrollView;
    
    y = 0;
    
    // beers
    UILabel *bioLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 4, 320, 30)];
    bioLabel.font = [UIFont fontWithName:@"Heiti SC" size:32];
    bioLabel.text = @"Beer";
    bioLabel.textColor = [UIColor whiteColor];
    bioLabel.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:bioLabel];
    y += bioLabel.frame.size.height;
    
    UIImageView *separator = [[UIImageView alloc] initWithFrame:CGRectMake(4, y, 320, 4)];
    separator.image = [UIImage imageNamed:@"details-separator.png"];
    [scrollView addSubview:separator];
    y += separator.frame.size.height + 10;
    
    for (int i = 0;i < [self->beers count];++i)
    {
        VenueBeer *beer = [self->beers objectAtIndex:i];
        NSString *priceStr = [NSString stringWithFormat:@"$%.2f", beer.beer_price.floatValue];
        [self addItemSectionWithName:priceStr withText:beer.beer_name addSeparator:(i == [self->beers count] - 1)];
    }
    y += 20;
    
    // wines
    UILabel *winesLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, y, 320, 30)];
    winesLabel.font = [UIFont fontWithName:@"Heiti SC" size:32];
    winesLabel.text = @"Wine";
    winesLabel.textColor = [UIColor whiteColor];
    winesLabel.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:winesLabel];
    y += winesLabel.frame.size.height;
    
    UIImageView *s2 = [[UIImageView alloc] initWithFrame:CGRectMake(4, y, 320, 4)];
    s2.image = [UIImage imageNamed:@"details-separator.png"];
    [scrollView addSubview:s2];
    y += s2.frame.size.height + 10;
    
    for (int i = 0;i < [self->wines count];++i)
    {
        VenueWine *wine = [self->wines objectAtIndex:i];
        NSString *priceStr = [NSString stringWithFormat:@"$%.2f", wine.glass_price.floatValue];
        [self addItemSectionWithName:priceStr withText:wine.wine_name];
    }
    
    //[self addItemSectionWithName:@"$4" withText:@"Coors Lite, Miller Lite, BudLite"];
    //[self addItemSectionWithName:@"$5" withText:@"Dos XX, Shiner Bock, Fireman's"];
    //[self addItemSectionWithName:@"$6" withText:@"Stella, Blue Moon" addSeparator:NO];
    
    scrollView.contentSize = CGSizeMake(320, y);
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 5, 0, 5);
}

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text
{
    [self addItemSectionWithName:name withText:text addSeparator:YES];
}

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text addSeparator:(BOOL)addSeparator
{    
    // "key" label
    UILabel *keyLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, y - 3, 100, 20)];
    keyLabel.font = [UIFont fontWithName:@"Heiti SC" size:17];
    keyLabel.text = name;
    keyLabel.textColor = [UIColor lightGrayColor];
    keyLabel.backgroundColor = [UIColor clearColor];
    keyLabel.textAlignment = UITextAlignmentRight;
    [scrollView addSubview:keyLabel];
    //y += descriptionLabel.frame.size.height;
    
    // "value" label
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(115, y, 195, 60)];
    textLabel.font = [UIFont fontWithName:@"Heiti SC" size:15];
    
    textLabel.numberOfLines = 4;
    textLabel.lineBreakMode = UILineBreakModeWordWrap;
    CGSize expectedLabelSize = [text sizeWithFont:textLabel.font constrainedToSize:CGSizeMake(205, 300) lineBreakMode:textLabel.lineBreakMode];   
    
    //adjust the label the the new height.
    CGRect newFrame = textLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    textLabel.frame = newFrame;
    textLabel.text = text;
    textLabel.textColor = [UIColor whiteColor];
    textLabel.backgroundColor = [UIColor clearColor];
    
    [scrollView addSubview:textLabel];
    y += textLabel.frame.size.height + 4;
    
    if (addSeparator)
    {
        UIImageView *separator = [[UIImageView alloc] initWithFrame:CGRectMake(4, y, 320, 4)];
        separator.image = [UIImage imageNamed:@"details-separator.png"];
        [scrollView addSubview:separator];
        y += separator.frame.size.height + 10;
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.menu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"VenueMenuCell";
    
    VenueMenuTableViewCell *cell = (VenueMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[VenueMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    VenueMenuItem *menuItem = [self.menu objectAtIndex:indexPath.row];
    cell.name.text = menuItem.name;
    cell.description.text = menuItem.description;
    cell.price.text = [NSString stringWithFormat:@"%.02f", menuItem.price];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
