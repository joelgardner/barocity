//
//  VenueMenuItem.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/13/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueMenuItem.h"

@implementation VenueMenuItem

@synthesize _id = __id, name = _name, description = _description, price = _price;

@end

