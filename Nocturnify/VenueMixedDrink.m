//
//  VenueMixedDrink.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VenueMixedDrink.h"


@implementation VenueMixedDrink

@dynamic venue_id;
@dynamic liquor_type;
@dynamic mixed_drink_name;
@dynamic single_price;
@dynamic double_indicator;
@dynamic double_price;
@dynamic mixed_drink_id;

@end
