//
//  VenueFilterContainerViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 10/28/12.
//
//

#import <UIKit/UIKit.h>

@interface VenueFilterContainerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *filterData;

- (id)initWithFilterData:(NSMutableArray *)filterData;

@end
