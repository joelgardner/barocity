//
//  VenueFilterContainerViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 10/28/12.
//
//

#import "VenueFilterContainerViewController.h"
#import "VenueFilterData.h"
#import "VenueFilterContainerTableViewCell.h"

@interface VenueFilterContainerViewController ()

@end

@implementation VenueFilterContainerViewController
{
    int y;
    UIScrollView *scrollView;
    UITableView *tableView;
}
@synthesize filterData = _filterData;
- (id)initWithFilterData:(NSMutableArray *)filterData
{
    self = [super init];
    if (self)
    {
        self.filterData = filterData;
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)loadView
{
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 0)];
    scrollView.showsVerticalScrollIndicator = YES;
    //self.view = scrollView;
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 400)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorColor = [UIColor darkGrayColor];
    
    self.view = tableView;
    [tableView reloadData];
    /*y = 10;
    for (VenueFilterData *data in self.filterData)
    {
        
        [self addItemSectionWithName:@"" withText:data.feature.feature_name];
    }
    [self addItemSectionWithName:@"" withText:@"" addSeparator:NO];
    */
    
    scrollView.contentSize = CGSizeMake(320, tableView.contentSize.height);
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 5, 0, 5);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.filterData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VenueFilterContainerTableViewCell *cell = (VenueFilterContainerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FilterContainerCell"];
    VenueFilterData *filterData = [self.filterData objectAtIndex:indexPath.row];
    if (cell == nil)
    {
        cell = [[VenueFilterContainerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilterContainerCell"];
    }
    
    cell.featureName.text = filterData.feature.feature_name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VenueFilterContainerTableViewCell *cell = (VenueFilterContainerTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.userSelected = !cell.userSelected;
}

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text
{
    [self addItemSectionWithName:name withText:text addSeparator:YES];
}

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text addSeparator:(BOOL)addSeparator
{
    // "key" label
    UILabel *keyLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, y - 3, 100, 20)];
    keyLabel.font = [UIFont fontWithName:@"Heiti SC" size:17];
    keyLabel.text = name;
    keyLabel.textColor = [UIColor lightGrayColor];
    keyLabel.backgroundColor = [UIColor clearColor];
    keyLabel.textAlignment = UITextAlignmentRight;
    [scrollView addSubview:keyLabel];
    //y += descriptionLabel.frame.size.height;
    
    // "value" label
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(115, y, 195, 60)];
    textLabel.font = [UIFont fontWithName:@"Heiti SC" size:15];
    
    textLabel.numberOfLines = 4;
    textLabel.lineBreakMode = UILineBreakModeWordWrap;
    CGSize expectedLabelSize = [text sizeWithFont:textLabel.font constrainedToSize:CGSizeMake(205, 300) lineBreakMode:textLabel.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = textLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    textLabel.frame = newFrame;
    textLabel.text = text;
    textLabel.textColor = [UIColor whiteColor];
    textLabel.backgroundColor = [UIColor clearColor];
    
    [scrollView addSubview:textLabel];
    y += textLabel.frame.size.height + 40;
    
    if (addSeparator)
    {
        UIImageView *separator = [[UIImageView alloc] initWithFrame:CGRectMake(4, y, 320, 4)];
        separator.image = [UIImage imageNamed:@"details-separator.png"];
        [scrollView addSubview:separator];
        y += separator.frame.size.height + 10;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
