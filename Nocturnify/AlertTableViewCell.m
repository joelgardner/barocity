//
//  AlertTableViewCell.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/18/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "AlertTableViewCell.h"

@implementation AlertTableViewCell

@synthesize name = _name, description = _description;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
