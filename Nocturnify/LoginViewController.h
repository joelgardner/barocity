//
//  LoginViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/22/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SyncController.h"
#import "CustomTextField.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate, SyncControllerDelegate>

@property (nonatomic, strong) IBOutlet CustomTextField* username;
@property (nonatomic, strong) IBOutlet CustomTextField* password;
@property (nonatomic, strong) IBOutlet UIView *waitScreen;

- (IBAction)verifyUsernameAndPassword;

@end
