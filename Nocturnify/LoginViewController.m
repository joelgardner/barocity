//
//  LoginViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/22/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "LoginViewController.h"
#import "SyncController.h"
#import "Login.h"
#import "RootViewController.h"
#import "LogHelper.h"
#import "SyncResult.h"
#import "Config.h"

@interface LoginViewController (PrivateMethods)

- (void) showWaitScreen;
- (void) hideWaitScreen;

@end

@implementation LoginViewController
{
    SyncController* syncController;
}

@synthesize username = _username, password = _password, waitScreen = _waitScreen;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        syncController = [[SyncController alloc] init];
        syncController.delegate = self;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor *color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-gunmetal.jpg"]];
    [self.view setBackgroundColor:color];
    
    // start syncing location
    [syncController syncLocation];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)verifyUsernameAndPassword
{
    [self showWaitScreen];
    
    if (YES || [self.username hasText] && [self.password hasText])
    {
        Login* login = [syncController verifyUsername:self.username.text withPassword:self.password.text];
        if (login.isSuccessful)
        {
            //[syncController sync];
        }
    }

}

- (void)syncDidFinish:(SyncResult *)syncResult
{
    if (syncResult.syncStatus == SYNC_SUCCEEDED)
    {
        // send the device id to the server
        [syncController syncDeviceId:[Config deviceId]];
        [self performSegueWithIdentifier:@"LogInSegue" sender:self];
    }
    else
    {   // sync failed
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Whoops"
                                                             message:@"We're having trouble.  Working on it."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
    }
    
    //[self hideWaitScreen];
}

- (void)locationUpdatedWithLatitude:(double)latitude withLongitude:(double)longitude
{
    [Config setLocation:[[CLLocation alloc] initWithLatitude:latitude longitude:longitude]];
    [syncController sync];
}

- (void)showWaitScreen
{
    self.waitScreen.hidden = NO;
    self.waitScreen.alpha = 0.8;
}

- (void)hideWaitScreen
{
    self.waitScreen.alpha = 0.0;
    self.waitScreen.hidden = YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
