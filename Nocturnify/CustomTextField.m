//
//  CustomTextField.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/28/12.
//
//

#import "CustomTextField.h"

@implementation CustomTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) drawPlaceholderInRect:(CGRect)rect
{
    [[UIColor colorWithRed:0.75f green:0.75f blue:0.75f alpha:0.5f] setFill];
    //[[self.searchField placeholder] drawInRect:rect withFont:[UIFont systemFontOfSize:13]];
    rect.origin.y += 1;
    rect.origin.x -= 1;
    [[self placeholder] drawInRect:rect withFont:[UIFont fontWithName:@"Heiti SC" size: 17] lineBreakMode:UILineBreakModeTailTruncation alignment:UITextAlignmentLeft];
}

@end
