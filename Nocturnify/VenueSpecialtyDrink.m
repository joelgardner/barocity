//
//  VenueSpecialtyDrink.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VenueSpecialtyDrink.h"


@implementation VenueSpecialtyDrink

@dynamic venue_id;
@dynamic s_drink_id;
@dynamic s_drink_name;
@dynamic s_drink_desc;
@dynamic s_drink_price;

@end
