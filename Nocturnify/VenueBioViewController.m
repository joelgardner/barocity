//
//  VenueBioViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/5/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueBioViewController.h"

@interface VenueBioViewController ()

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text;
- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text addSeparator:(BOOL)separator;
@end

@implementation VenueBioViewController
{
    int y;
    UIScrollView *scrollView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 0)];
    scrollView.showsVerticalScrollIndicator = YES;
    self.view = scrollView;

    y = 0;
    UILabel *bioLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 4, 320, 30)];
    bioLabel.font = [UIFont fontWithName:@"Heiti SC" size:32];
    bioLabel.text = @"Bio";
    bioLabel.textColor = [UIColor whiteColor];
    bioLabel.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:bioLabel];
    y += bioLabel.frame.size.height;
    
    UIImageView *separator = [[UIImageView alloc] initWithFrame:CGRectMake(4, y, 320, 4)];
    separator.image = [UIImage imageNamed:@"details-separator.png"];
    [scrollView addSubview:separator];
    y += separator.frame.size.height + 10;
    
    [self addItemSectionWithName:@"Phone" withText:@"(512) 833-3928"];
    [self addItemSectionWithName:@"Description" withText:@"This is where the description will go.  This is where the description will go.  Yeah."];
    [self addItemSectionWithName:@"Website" withText:@"barlouie.net"];
    [self addItemSectionWithName:@"Facebook" withText:@"facebook.com/hangar"];
    [self addItemSectionWithName:@"1" withText:@"one"];
    [self addItemSectionWithName:@"2" withText:@"two"];
    [self addItemSectionWithName:@"3" withText:@"three"];
    [self addItemSectionWithName:@"4" withText:@"four"];
    [self addItemSectionWithName:@"5" withText:@"five"];
    [self addItemSectionWithName:@"6" withText:@"six"];
    [self addItemSectionWithName:@"7" withText:@"seven"];
    [self addItemSectionWithName:@"8" withText:@"eight"];
    [self addItemSectionWithName:@"Twitter" withText:@"twitter.com/asdasd" addSeparator:NO];
    
    scrollView.contentSize = CGSizeMake(320, y);
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 5, 0, 5);
}

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text
{
    [self addItemSectionWithName:name withText:text addSeparator:YES];
}

- (void)addItemSectionWithName:(NSString *)name withText:(NSString *)text addSeparator:(BOOL)addSeparator
{    
    // "key" label
    UILabel *keyLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, y - 3, 100, 20)];
    keyLabel.font = [UIFont fontWithName:@"Heiti SC" size:17];
    keyLabel.text = name;
    keyLabel.textColor = [UIColor lightGrayColor];
    keyLabel.backgroundColor = [UIColor clearColor];
    keyLabel.textAlignment = UITextAlignmentRight;
    [scrollView addSubview:keyLabel];
    //y += descriptionLabel.frame.size.height;
    
    // "value" label
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(115, y, 195, 60)];
    textLabel.font = [UIFont fontWithName:@"Heiti SC" size:15];
    
    textLabel.numberOfLines = 4;
    textLabel.lineBreakMode = UILineBreakModeWordWrap;
    CGSize expectedLabelSize = [text sizeWithFont:textLabel.font constrainedToSize:CGSizeMake(205, 300) lineBreakMode:textLabel.lineBreakMode];   
    
    //adjust the label the the new height.
    CGRect newFrame = textLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    textLabel.frame = newFrame;
    textLabel.text = text;
    textLabel.textColor = [UIColor whiteColor];
    textLabel.backgroundColor = [UIColor clearColor];

    [scrollView addSubview:textLabel];
    y += textLabel.frame.size.height + 4;
    
    if (addSeparator)
    {
        UIImageView *separator = [[UIImageView alloc] initWithFrame:CGRectMake(4, y, 320, 4)];
        separator.image = [UIImage imageNamed:@"details-separator.png"];
        [scrollView addSubview:separator];
        y += separator.frame.size.height + 10;
    }
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
