//
//  VenueDetailViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/14/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Venue.h"

@interface VenueDetailViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) Venue *venue;
@property (nonatomic, strong) IBOutlet UILabel *name;
@property (nonatomic, strong) IBOutlet UILabel *address;
@property (nonatomic, strong) IBOutlet UILabel *description;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) IBOutlet UILabel *menuItemsCount;
@property (nonatomic, strong) IBOutlet UILabel *featuresCount;
@property (nonatomic, strong) IBOutlet UILabel *dealsCount;
- (IBAction)bigButtonPressed:(id)sender;

- (IBAction)pageChanged:(id)sender;
@end
