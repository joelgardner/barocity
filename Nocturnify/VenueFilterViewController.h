//
//  VenueFilterViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/27/12.
//
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"

@interface VenueFilterViewController : UIViewController <UITextFieldDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet CustomTextField *searchField;
@property (nonatomic, strong) NSMutableArray *features;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

@end
