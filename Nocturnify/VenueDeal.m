//
//  VenueDeal.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/10/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueDeal.h"

@implementation VenueDeal

@synthesize _id = __id, title = _title, description = _description;

@end
