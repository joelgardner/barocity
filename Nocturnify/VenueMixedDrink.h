//
//  VenueMixedDrink.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface VenueMixedDrink : NSManagedObject

@property (nonatomic, retain) NSNumber * venue_id;
@property (nonatomic, retain) NSString * liquor_type;
@property (nonatomic, retain) NSString * mixed_drink_name;
@property (nonatomic, retain) NSDecimalNumber * single_price;
@property (nonatomic, retain) NSDecimalNumber * double_indicator;
@property (nonatomic, retain) NSDecimalNumber * double_price;
@property (nonatomic, retain) NSNumber * mixed_drink_id;

@end
