//
//  RootViewController.h
//  Nocturnify
//
//  Created by Joel Gardner on 6/24/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITabBarController <UITabBarDelegate>

@end
