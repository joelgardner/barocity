//
//  Config.h
//  Barocity
//
//  Created by Joel Gardner on 11/20/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Config : NSObject

+ (NSString *)deviceId;
+ (void)setDeviceId:(NSData *)data;

+ (CLLocation *)location;
+ (void)setLocation:(CLLocation *)location;

@end
