//
//  SyncData.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/12/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "SyncData.h"

@implementation SyncData

@synthesize  data = _data, jsonKey = _jsonKey, saveObjectsSelector = _saveObjectsSelector, saveData = _saveData, url = _url;

- (SyncData *)initWithJsonKey:(NSString *)jsonKey withURL:(NSURL *)url withSaveDataBlock:(void (^)(NSArray *data))saveData
{
    self = [super init];
    if (self)
    {
        self.jsonKey = jsonKey;
        self.url = url;
        self.saveData = saveData;
    }
    return self;
}


- (SyncData *)initWithJsonKey:(NSString *)jsonKey withURL:(NSURL *)url withSaveObjectsSelector:(SEL)saveObjectsSelector
{
    self = [super init];
    if (self)
    {
        self.jsonKey = jsonKey;
        self.url = url;
        self.saveObjectsSelector = saveObjectsSelector;
    }
    return self;
}
@end
