//
//  VenueFilterContainerTableViewCell.m
//  Barocity
//
//  Created by Joel Gardner on 11/13/12.
//
//

#import "VenueFilterContainerTableViewCell.h"

@implementation VenueFilterContainerTableViewCell
{
    UIImageView *imgSelected;
    BOOL _userSelected;
}

@synthesize featureName = _featureName, userSelected;

- (void)setUserSelected:(BOOL)selected
{
    userSelected = selected;
    if (selected)
    {
        imgSelected.frame = CGRectMake(13.5, 15, 28, 28);
        imgSelected.image = [UIImage imageNamed:@"27-circle-o@2x.png"];
        //imgSelected.alpha = 1.00;
    }
    else
    {
        imgSelected.frame = CGRectMake(20, 22, 15, 15);
        imgSelected.image = [UIImage imageNamed:@"47-o@2x.png"];
        //imgSelected.alpha = 0.75;
    }
}

- (BOOL)userSelected
{
    return userSelected;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.contentView.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        /* the following code should probably be in layoutSubviews */
        
        // add dot picture
        imgSelected = [[UIImageView alloc] init];
        self.userSelected = NO;
        [self addSubview:imgSelected];
        
        // add label
        self.featureName = [[UILabel alloc] initWithFrame:CGRectMake(80, 22, 200, 20)];
        self.featureName.text = @"Barbarella";
        self.featureName.font = [UIFont fontWithName:@"Heiti SC" size:20.0];
        self.featureName.backgroundColor = [UIColor clearColor];
        self.featureName.textColor = [UIColor lightTextColor];
        [self addSubview:self.featureName];
    }
    return self;
}

@end
