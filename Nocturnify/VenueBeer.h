//
//  VenueBeer.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/9/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface VenueBeer : NSManagedObject

@property (nonatomic, retain) NSNumber * venue_id;
@property (nonatomic, retain) NSNumber * beer_id;
@property (nonatomic, retain) NSString * beer_name;
@property (nonatomic, retain) NSString * beer_type;
@property (nonatomic, retain) NSString * form_type;
@property (nonatomic, retain) NSDecimalNumber * beer_price;
@property (nonatomic, retain) NSDecimalNumber * bucket_price;
@property (nonatomic, retain) NSString * beer_type_desc;

@end
