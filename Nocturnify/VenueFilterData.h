//
//  VenueFilterData.h
//  Nocturnify
//
//  Created by Joel Gardner on 10/28/12.
//
//

#import <Foundation/Foundation.h>
#import "Feature.h"

@interface VenueFilterData : NSObject
@property (nonatomic, strong) Feature *feature;
@property int search_value;

- (id)initWithFeature:(Feature *)f;

@end
