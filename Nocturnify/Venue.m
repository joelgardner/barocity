//
//  Venue.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/9/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "Venue.h"


@implementation Venue

@dynamic address;
@dynamic city;
@dynamic description_;
@dynamic id_;
@dynamic name;
@dynamic phone;
@dynamic state;
@dynamic zip;

@end
