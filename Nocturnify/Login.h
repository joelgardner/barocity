//
//  Login.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/22/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Login : NSObject

@property int syncId;
@property BOOL isSuccessful;

@end
