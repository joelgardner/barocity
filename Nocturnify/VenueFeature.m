//
//  VenueFeature.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/10/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueFeature.h"

@implementation VenueFeature

@synthesize _id = __id, title = _title, description = _description, count = _count;

@end
