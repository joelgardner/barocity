//
//  Feature.h
//  Nocturnify
//
//  Created by Joel Gardner on 8/27/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Feature : NSManagedObject

@property (nonatomic, retain) NSNumber * id_;
@property (nonatomic, retain) NSString * feature_name;
@property (nonatomic, retain) NSString * feature_type;

@end
