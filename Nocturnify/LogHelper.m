//
//  LogHelper.m
//  Nocturnify
//
//  Created by Joel Gardner on 8/12/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "LogHelper.h"
#import "Venue.h"

@implementation LogHelper

+ (void)logVenue:(Venue *)venue
{
    NSLog(@"%@", venue.name);
}

+ (void)logMessage:(NSString *)message
{
    NSLog(@"%@", message);
}

@end
