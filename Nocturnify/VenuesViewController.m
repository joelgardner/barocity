//
//  VenuesViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/2/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenuesViewController.h"
#import "VenueTableViewCell.h"
#import "Venue.h"
#import "DataController.h"
#import "VenueDetailViewController.h"
#import "VenueFilterViewController.h"
#import "LogHelper.h"

@implementation VenuesViewController
{
    DataController *dataController;
    UIBarButtonItem *doneButton;
    VenueFilterViewController *filterViewController;
}

@synthesize venues = _venues, filteredVenues = _filteredVenues, searchBar = _searchBar;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        dataController = [[DataController alloc] init];
        filterViewController = [[VenueFilterViewController alloc] init];
        self.venues = [dataController getVenues];
        NSLog(@"num venues: %d", [self.venues count]);
    }

    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{

}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UIColor clearColor];

    [self.searchBar setBackgroundImage:[UIImage imageNamed:@"bg-gunmetal.jpg"]];
    UIColor *color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-gunmetal.jpg"]];
    [self.view setBackgroundColor:color];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"Venues";
    titleLabel.font = [UIFont fontWithName:@"Heiti SC" size: 22.0];
    titleLabel.textAlignment = UITextAlignmentCenter;
    self.navigationItem.titleView = titleLabel;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.venues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Venue *venue = [self.venues objectAtIndex:indexPath.row];
    VenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VenueCell"];
    cell.name.text = venue.name;
    cell.address.text = venue.address;
    cell.description.text = venue.description_;
    cell.image.image = [UIImage imageNamed:@"austin-sixth-street.jpeg"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 77;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (doneButton == nil)
    {
        doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard)];
        doneButton.tintColor = [UIColor grayColor];
    }
    self.navigationItem.rightBarButtonItem = doneButton;
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self dismissKeyboard];
    if (searchBar.text.length > 0)
    {
        self.filteredVenues = [dataController getVenuesWithSearchTerm:searchBar.text];
    }
    // TODO: implement search logic
}

- (void)dismissKeyboard
{
    [self.searchBar resignFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"VenueDetail"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        VenueDetailViewController *detailsController = (VenueDetailViewController *)[segue destinationViewController];
        detailsController.venue = [self.venues objectAtIndex:indexPath.row];
    }
    else if ([[segue identifier] isEqualToString:@"FilterSegue"])
    {
        VenueFilterViewController *filterController = (VenueFilterViewController *)[segue destinationViewController];
        filterController.features = [dataController getAllFeatures];
    }
}

@end
