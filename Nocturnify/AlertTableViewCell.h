//
//  AlertTableViewCell.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/18/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *name;
@property (nonatomic, strong) IBOutlet UILabel *description;
//@property (nonatomic, strong) IBOutlet UILabel *whatever;

@end
