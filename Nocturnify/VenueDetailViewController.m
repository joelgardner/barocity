//
//  VenueDetailViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 7/14/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import "VenueDetailViewController.h"
#import "VenueDealsViewController.h"
#import "VenueFeaturesViewController.h"
#import "VenueMenuViewController.h"
#import "VenueBioViewController.h"
#import "DataController.h"

@interface VenueDetailViewController (PrivateMethods)
- (void)loadScrollViewWithPage:(int)page;
- (void)scrollViewDidScroll:(UIScrollView *)sender;
@end

@implementation VenueDetailViewController
{
    BOOL pageControlUsed;
    DataController *dataController;
    VenueBioViewController *bioViewController;
    VenueMenuViewController *menuViewController;
    VenueFeaturesViewController *featureViewController;
    VenueDealsViewController *dealViewController;
}

@synthesize venue = _venue, name = _name, address = _address, description = _description, imageView = _imageView, scrollView = _scrollView, pageControl = _pageControl, menuItemsCount = _menuItemsCount, featuresCount = _featuresCount, dealsCount = _dealsCount;



- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        dataController = [[DataController alloc] init];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{

}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    self.name.text = self.venue.name;
    self.address.text = self.venue.address;
    self.description.text = self.venue.description_;
    
    UIColor *color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-gunmetal.jpg"]];
    [self.view setBackgroundColor:color];
    
    // a page is the width of the scroll view
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 4, self.scrollView.frame.size.height - 95);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    // set up page control
    self.pageControl.numberOfPages = 4;
    self.pageControl.currentPage = 0;
    
    [self loadScrollViewWithPage:0];

    [super viewDidLoad];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"VenueDeals"])
    {
        //VenueDealsViewController *dealsController = (VenueDealsViewController *)[segue destinationViewController];
        //dealsController.deals = self.venue.deals;
    }
    else if ([[segue identifier] isEqualToString:@"VenueFeatures"])
    {
        //VenueFeaturesViewController *featuresController = (VenueFeaturesViewController *)[segue destinationViewController];
        //featuresController.features = self.venue.features;
    }
    else if ([[segue identifier] isEqualToString:@"VenueMenu"])
    {
        //VenueMenuViewController *menuController = (VenueMenuViewController *)[segue destinationViewController];
        //menuController.menu = self.venue.menu;
    }
}
*/
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // A possible optimization would be to unload the views+controllers which are no longer visible
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

- (IBAction)pageChanged:(id)sender
{
    int page = self.pageControl.currentPage;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0 || page > 3)
        return;
    
    if (page == 0 && bioViewController == nil)
    {
        // testing
        bioViewController = [[VenueBioViewController alloc] init];
        bioViewController.view.frame = CGRectMake(self.scrollView.frame.size.width * page, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height - 95);
        [self.scrollView addSubview:bioViewController.view];
    }
    else if (page == 1 && menuViewController == nil)
    {
        NSMutableArray *beers = [dataController getBeersForVenue:[self.venue.id_ intValue]];
        NSMutableArray *wines = [dataController getWinesForVenue:[self.venue.id_ intValue]];
        NSMutableArray *mixedDrinks = [dataController getMixedDrinksForVenue:[self.venue.id_ intValue]];
        NSMutableArray *specialtyDrinks = [dataController getSpecialtyDrinksForVenue:[self.venue.id_ intValue]];
        
        menuViewController = [[VenueMenuViewController alloc] initWithBeers:beers
                                                                  withWines:wines
                                                            withMixedDrinks:mixedDrinks
                                                        withSpecialtyDrinks:specialtyDrinks];
        
        
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        menuViewController.view.frame = frame;
        [self.scrollView addSubview:menuViewController.view];
    }
    else if (page == 2 && featureViewController == nil)
    {
        featureViewController = [[VenueFeaturesViewController alloc] init];
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        featureViewController.view.frame = frame;
        [self.scrollView addSubview:featureViewController.view];
    }
    else if (page == 3 && dealViewController == nil)
    {
        dealViewController = [[VenueDealsViewController alloc] init];
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        dealViewController.view.frame = frame;
        [self.scrollView addSubview:dealViewController.view];
    }
}

- (IBAction)bigButtonPressed:(id)sender 
{
    self.pageControl.currentPage = ((UIButton *)sender).tag;
    [self pageChanged:nil];
}

@end
