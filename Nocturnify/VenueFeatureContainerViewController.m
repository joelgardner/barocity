//
//  VenueFeatureContainerViewController.m
//  Nocturnify
//
//  Created by Joel Gardner on 10/28/12.
//
//

#import "VenueFeatureContainerViewController.h"

@interface VenueFeatureContainerViewController ()

@end

@implementation VenueFeatureContainerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
