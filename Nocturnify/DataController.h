//
//  DataController.h
//  Nocturnify
//
//  Created by Joel Gardner on 7/13/12.
//  Copyright (c) 2012 Nocturnify. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataController : NSObject

- (NSMutableArray *)getVenues;
- (NSMutableArray *)getDeals:(int) _venueId;
- (NSMutableArray *)getAttributes:(int) _venueId;
- (NSMutableArray *)getMenu:(int) _venueId;
- (NSMutableArray *)getAlerts;
- (NSMutableArray *)getVenuesWithSearchTerm:(NSString *)searchTerm;
- (NSMutableArray *)getBeersForVenue:(int)_venueId;
- (NSMutableArray *)getWinesForVenue:(int)_venueId;
- (NSMutableArray *)getMixedDrinksForVenue:(int)_venueId;
- (NSMutableArray *)getSpecialtyDrinksForVenue:(int)_venueId;
- (NSMutableArray *)getAllFeatures;
- (NSMutableArray *)getGeneralFeatures;

- (void)clearVenues;
- (void)saveVenues:(NSArray *)venues;
- (void)clearVenueBeers;
- (void)saveVenueBeers:(NSArray *)venueBeers;
- (void)clearVenueWines;
- (void)saveVenueWines:(NSArray *)venueWines;
- (void)clearVenueMixedDrinks;
- (void)saveVenueMixedDrinks:(NSArray *)venueMixedDrinks;
- (void)clearVenueSpecialtyDrinks;
- (void)saveVenueSpecialtyDrinks:(NSArray *)venueSpecialtyDrinks;
- (void)clearFeatures;
- (void)saveFeatures:(NSArray *)features;
@end
